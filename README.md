# Web-Simulation



## Getting started

Pour mettre en place le serveur Web de Simulation :

* Déposer le git dans une instance Apache fonctionnant sous PHP >7.4

* Installer les dépendances composer : composer install 

* Configurer le fichier .env pour correspondre aux paramètres de la BDD :
' DATABASE_URL="postgresql://USER_BDD:PASS_USER@HOST_BDD:5432/NOM_BDD?serverVersion=VERSION_POSTGRESQL&charset=utf8" '

* Mettre en place la base de données via Doctrine (ORM) : php bin/console doctrine:migrations:migrate

* Clear cache : php bin/console cache:clear

Accéder à l'application via : http://HOST/ServeurWebA/public/index.php/
-> attention tant qu'il n'y a pas de .htaccess pensez à bien mettre le dernier / sinon les requetes AJAX de l'IHM ne fonctionneront pas.

## Use the API

L'API est disponible ici : https://simulation.grp2.swano-lab.net/

* Il est possible d'effectuer des actions sur les capteurs via l'API Sensors :

- {GET} /sensor renvoie la liste des sensor au format JSON
- {POST} /sensor/new permet de créer un sensor en passant les coordonnées en paramètres JSON de la requête : x (float), y (float), z (integer) et etat (integer). La requête renverra false si une erreur de données entrées et présentes ou si un capteur est déjà déclaré à cette position.
- {GET} /sensor/ID renvoie les données d'un sensor au format JSON
- {PUT} /sensor/ID/etat permet de modifier l'état d'un sensor à partir des paramètres JSON etat (integer) et date (string) au format Y-m-d H:i:s 
- {DELETE} /sensor/ID permet de supprimer un sensor avec pour paramètre delete (string) qui correspond à la chaîne concaténée de "delete" avec l'ID du capteur.

- {GET} /fire renvoie la liste des sensors dont l'état est supérieur à 0.
